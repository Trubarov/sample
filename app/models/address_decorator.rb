Spree::Address.class_eval do
  has_one :shop, :inverse_of => :location

  _validators.reject!{ |key, value| [:address1, :city, :country].include? key }
  _validate_callbacks.each do |callback|
    callback.raw_filter.attributes.reject! { |key| [:address1, :city, :country].include? key } if callback.raw_filter.respond_to?(:attributes)
  end

  def require_phone?
    true
  end

  def require_zipcode?
    false
  end

  def address
    "#{address1} #{city} #{country}"
  end

  def street
    address1
  end

  def number
    address2
  end

  def country_name
    country.name unless country.nil?
  end

end