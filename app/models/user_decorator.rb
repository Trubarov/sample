Spree::User.class_eval do

  has_one :shop
  accepts_nested_attributes_for :shop

  def self.guest
    where(email: 'guest@example.net').first
  end

  def anonymous?
    email =~ /@example.net$/ ? true : false
  end

end
