Spree::Variant.class_eval do
  has_one :default_price,
          -> { where(currency: Spree::Config[:currency], shop_id: nil) },
          class_name: 'Spree::Price',
          dependent: :destroy

  def shop_price(shop)
    prices.where(shop_id: shop.id).first
  end

  def display_shop_price(shop)
    shop_price(shop).display_price
  end
end