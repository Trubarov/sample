class AbilityDecorator

  include CanCan::Ability

  def initialize(user)

    user ||= Spree.user_class.new

    if user.respond_to?(:has_spree_role?) && user.has_spree_role?('admin')
      cannot [:stock, :shop], Spree::Product
    end

    if user.respond_to?(:has_spree_role?) && user.has_spree_role?('manager')
      can [:admin, :shop], Spree::Product
      can [:admin, :index], Spree::Order, :shop_id => user.shop.id
    end

    can [:display], Shop

  end

end

Spree::Ability.register_ability(AbilityDecorator)
