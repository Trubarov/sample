class ProductsShops < ActiveRecord::Base

  belongs_to :product, :class_name => 'Spree::Product'
  belongs_to :shop, :class_name => 'Shop'

end