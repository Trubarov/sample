Spree::Product.class_eval do

  has_many :products_shops, :class_name => 'ProductsShops'
  has_many :shops, :through => :products_shops

  delegate_belongs_to :master, :shop_price, :display_shop_price

  def set_shop_price(shop, price)
    price_record = shop_price(shop)
    price_record.amount = price
    price_record.save
  end

end
