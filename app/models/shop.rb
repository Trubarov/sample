class Shop < ActiveRecord::Base

  has_many :products_shops, :class_name => 'ProductsShops'
  has_many :products, :through => :products_shops

  has_many :prices, :class_name => 'Spree::Price'

  has_many :orders, :class_name => 'Spree::Order'

  belongs_to :user, :class_name => 'Spree::User'

  belongs_to :location, :class_name => 'Spree::Address', :foreign_key => :address_id

  after_create :create_products

  has_attached_file :photo,
                    styles: { small: '100x100>', normal: '500x500>' },
                    default_style: :small,
                    url: '/spree/shops/:id/:style/:basename.:extension',
                    path: ':rails_root/public/spree/shops/:id/:style/:basename.:extension',
                    default_url: '/assets/default_shop.png'

  validates_attachment :photo,
                       content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"] }

  validates_presence_of :name

  delegate_belongs_to :location, :address

  geocoded_by :address
  after_validation :geocode

  def self.search(params)
    if params[:latitude].present? && params[:longitude].present?
      Shop.search_by_coordinates(params[:latitude], params[:longitude])
    elsif params[:address].present?
      Shop.search_by_address(params[:address])
    else
      Shop.all
    end
  end

  def rounded_distance_from(location)
    distance_from(location).round(2)
  end

  def opened?
    true
  end
  alias_method :opened, :opened?

  protected

  def self.search_by_coordinates(latitude, longitude)
    self.search_by([latitude, longitude])
  end

  def self.search_by_address(address)
    geocoded_location = Geokit::Geocoders::MultiGeocoder.geocode(address)
    self.search_by([geocoded_location.lat, geocoded_location.lng])
  end

  def self.search_by(location)
    Shop.near(location, Spree::Config[:shop_distance])
  end

  def create_products
    products = Spree::Product.all

    products.each do |product|
      shop_price = Spree::Price.new(:variant_id => product.master.id, :amount => product.price, :shop_id => self.id, :currency => Spree::Config[:currency])
      shop_price.save
    end
  end

end