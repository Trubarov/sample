Spree::Price.class_eval do
  belongs_to :shop, :inverse_of => :price, :dependent => :destroy
end