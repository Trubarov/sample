Deface::Override.new(:virtual_path => 'spree/admin/products/index',
                     :name => 'change_product_edit_link',
                     :replace => "erb[loud]:contains('edit_admin_product_path')",
                     :partial => 'admin/products/edit_link')

Deface::Override.new(:virtual_path => 'spree/admin/products/index',
                     :name => 'show_shop_product_price',
                     :replace => "erb[loud]:contains('display_price')",
                     :partial => 'admin/products/shop_price')

Deface::Override.new(:virtual_path => 'spree/admin/shared/_product_tabs',
                     :name => 'change_permission_for_product_edit',
                     :replace => "erb[silent]:contains(':admin, Spree::Product')",
                     :partial => 'admin/products/edit_tab_link')