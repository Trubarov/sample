Deface::Override.new(:virtual_path => 'spree/shared/_products',
                     :name => 'change_product_list_item_view',
                     :replace => "[data-hook='products_list_item']",
                     :partial => 'spree/products/list_item')

Deface::Override.new(:virtual_path => 'spree/products/_cart_form',
                     :name => 'change_product_price',
                     :replace => "[data-hook='product_price']",
                     :partial => 'spree/products/product_price')