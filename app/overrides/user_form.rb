Deface::Override.new(:virtual_path => 'spree/admin/users/_form',
                     :name => 'add_shops_select',
                     :insert_bottom => "[data-hook='admin_user_form_roles']",
                     :partial => 'admin/users/form_shops')
