Deface::Override.new(:virtual_path => 'spree/home/index',
                     :name => 'replace_products_with_shops',
                     :replace => "[data-hook='homepage_products']",
                     :partial => 'spree/home/shops')

Deface::Override.new(:virtual_path => 'spree/home/index',
                     :name => 'delete_sidebar_navigation',
                     :remove => "[data-hook='homepage_sidebar_navigation']")