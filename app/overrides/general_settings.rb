Deface::Override.new(:virtual_path => 'spree/admin/general_settings/edit',
                     :name => 'add_fields_to_general_settings',
                     :insert_top => 'div[id="preferences"]',
                     :partial => 'admin/general_settings/additional_fields')
