Deface::Override.new(:virtual_path => 'spree/admin/shared/_tabs',
                     :name => 'remove_promotions_tab',
                     :remove => "erb[loud]:contains('promotions')")

Deface::Override.new(:virtual_path => 'spree/admin/shared/_tabs',
                     :name => 'add_shops_tab',
                     :insert_after => "erb[loud]:contains('users')",
                     :partial => 'admin/shared/shops_tab')

Deface::Override.new(:virtual_path => 'spree/admin/shared/_product_tabs',
                     :name => 'add_manage_tab',
                     :insert_bottom => "[data-hook='admin_product_tabs']",
                     :partial => 'admin/products/manage_tab')
