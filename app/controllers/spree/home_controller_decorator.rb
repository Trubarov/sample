Spree::HomeController.class_eval do

  def index
    @shops = Shop.search(params)
  end

end