Spree::Api::LineItemsController.class_eval do
  def create
    variant = Spree::Variant.find(params[:line_item][:variant_id])
    @line_item = order.contents.add(variant, order.shop, params[:line_item][:quantity] || 1)

    if @line_item.errors.empty?
      @order.ensure_updated_shipments
      respond_with(@line_item, status: 201, default_template: :show)
    else
      invalid_resource!(@line_item)
    end
  end
end