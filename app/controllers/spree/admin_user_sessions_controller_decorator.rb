Spree::Admin::UserSessionsController.class_eval do
  def after_sign_in_path_for(resource)
    if (stored_location_for(resource) == admin_path || stored_location_for(resource).nil?) && resource.is_a?(Spree::User) && resource.has_spree_role?('manager')
      admin_products_path
    else
      stored_location_for(resource) || admin_path
    end
  end
end