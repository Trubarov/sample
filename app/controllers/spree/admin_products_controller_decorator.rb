Spree::Admin::ProductsController.class_eval do

  def shop
    if params[:product].present?
      if @object.set_shop_price(spree_current_user.shop, params[:product][:shop_price])
        invoke_callbacks(:update, :after)
        flash[:success] = flash_message_for(@object, :successfully_updated)
        respond_with(@object) do |format|
          format.html { redirect_to spree.shop_admin_product_url(@product) }
          format.js   { render :layout => false }
        end
      else
        invoke_callbacks(:update, :fails)
        respond_with(@object)
      end
    end
  end

end