Spree::Api::OrdersController.class_eval do

  def create
    authorize! :create, Spree::Order
    @order = Spree::Core::Importer::Order.import(current_api_user, order_params)
    @order.shop_id = params[:order][:shop_id]
    @order.save!
    respond_with(@order, default_template: :show, status: 201)
  end

  private

  def order_params
    if params[:order].present?
      params[:order][:payments_attributes] = params[:order][:payments] if params[:order][:payments]
      params[:order][:shipments_attributes] = params[:order][:shipments] if params[:order][:shipments]
      params[:order][:line_items_attributes] = params[:order][:line_items] if params[:order][:line_items]
      params[:order][:ship_address_attributes] = params[:order][:ship_address] if params[:order][:ship_address]
      params[:order][:bill_address_attributes] = params[:order][:bill_address] if params[:order][:bill_address]

      params.require(:order).permit(permitted_order_attributes).merge({:shop_id => params[:order][:shop_id]})
    else
      {}
    end
  end
end