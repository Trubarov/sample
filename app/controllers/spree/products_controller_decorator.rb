Spree::ProductsController.class_eval do

  def index
    @shops = Shop.search(params)
    render 'spree/home/index'
  end

  def show
    @shop = Shop.find(params[:shop_id])
    @variants = @product.variants_including_master.active(current_currency).includes([:option_values, :images])
    @product_properties = @product.product_properties.includes(:property)
    @taxon = Spree::Taxon.find(params[:taxon_id]) if params[:taxon_id]
  end

  private

  def accurate_title
    @product ? @product.name : super
  end

  def load_product
    if try_spree_current_user.try(:has_spree_role?, "admin")
      @products = Spree::Product.with_deleted
    else
      @products = Spree::Product.active(current_currency)
    end
    @product = @products.friendly.find(params[:id])
  end

  def load_taxon
    @taxon = Spree::Taxon.find(params[:taxon]) if params[:taxon].present?
  end
end