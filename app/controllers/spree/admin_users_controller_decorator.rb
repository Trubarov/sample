Spree::Admin::UsersController.class_eval do

  def update
    if params[:user]
      roles = params[:user].delete("spree_role_ids")
      shop = params[:user].delete('shop')
    end

    if @user.update_attributes(user_params)
      if roles
        @user.spree_roles = roles.reject(&:blank?).collect{|r| Spree::Role.find(r)}
      end

      if shop
        @user.shop = Shop.find(shop)
      end

      flash.now[:success] = Spree.t(:account_updated)
    end

    render :edit
  end

end