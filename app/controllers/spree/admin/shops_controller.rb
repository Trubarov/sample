module Spree
  module Admin
    class ShopsController < ResourceController

      before_filter :build_address, :only => [:new, :edit]
      before_filter :save_address, :only => [:create, :update]

      def index
      end

      protected

      def model_class
        "#{controller_name.classify}".constantize
      end

      def collection
        return @collection if @collection.present?
        params[:q] ||= {}
        params[:q][:deleted_at_null] ||= "1"

        params[:q][:s] ||= "name asc"
        @collection = super
        @collection = @collection.with_deleted if params[:q][:deleted_at_null] == '0'
        @search = @collection.ransack(params[:q])
        @collection = @search.result.
            page(params[:page]).
            #per(Spree::Config[:admin_shops_per_page])
            per(10)

        @collection
      end

      def build_address
        @shop.location ||= Spree::Address.default
      end

      def save_address
        @shop.location ||= Spree::Address.default
        @shop.location.update_attributes(address_params)
      end

      def permitted_resource_params
        params.require(:shop).permit!
      end

      def address_params
        params[:shop].delete(:location)
      end

    end
  end
end
