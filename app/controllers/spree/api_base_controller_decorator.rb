Spree::Api::BaseController.class_eval do

  private

  def authenticate_user
    unless @current_api_user
      if requires_authentication? && api_key.blank? && order_token.blank? && !guest_action?
        render "spree/api/errors/must_specify_api_key", :status => 401 and return
      elsif order_token.blank? && (requires_authentication? || api_key.present?) && !guest_action?
        render "spree/api/errors/invalid_api_key", :status => 401 and return
      else
        # An anonymous user
        @current_api_user = Spree.user_class.new
      end
    end
  end

  def guest_action?
    controller_name == 'users' && action_name == 'guest'
  end

end