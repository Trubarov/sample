module Spree
  class ShopsController < Spree::StoreController
    helper 'spree/products'
    respond_to :html

    def index
      @shops = Shop.search(params[:user])
      render 'spree/home/index'
    end

    def show
      @shop = Shop.find(params[:id])
      return unless @shop

      update_shop_reference

      @searcher = build_searcher(params.merge(:shop => @shop.id))
      @products = @searcher.retrieve_products
      @taxonomies = Spree::Taxonomy.includes(root: :children)
    end

    private

    def update_shop_reference
      if !current_order.nil? && (current_order.shop_id.nil? || current_order.shop_id != @shop.id)
        current_order.empty!
        current_order.shop_id = @shop.id
        current_order.save!
      end
    end
  end
end
