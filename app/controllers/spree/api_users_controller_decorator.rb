Spree::Api::UsersController.class_eval do

  def guest
    @user = Spree::User.guest
    respond_with(@user)
  end

  def create
    authorize! :create, Spree.user_class
    @user = Spree.user_class.new(user_params)
    if @user.save
      @user.generate_spree_api_key!
      respond_with(@user, :status => 201, :default_template => :show)
    else
      invalid_resource!(@user)
    end
  end

  private

  def user_params
    params.require(:user).permit(permitted_user_attributes + [:ship_address_attributes => permitted_address_attributes + additional_address_attributes])
  end

  def user
    @user ||= Spree.user_class.find(params[:id])
  end

  def additional_address_attributes
    [:entrance, :apartment, :floor, :delivery_instructions]
  end

end