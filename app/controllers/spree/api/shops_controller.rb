module Spree
  module Api
    class ShopsController < Spree::Api::BaseController
      def index
        @user_location = user_location(params[:user])
        @shops = Shop.accessible_by(current_ability, :read).search(params[:user])
        respond_with(@shops)
      end

      def show
        @shop = Shop.accessible_by(current_ability, :read).find(params[:id])
        @products = product_scope.ransack(params[:q]).result
      end

      private

      def user_location(params)
        if params[:latitude].present? && params[:longitude].present?
          [params[:latitude], params[:longitude]]
        elsif params[:address].present?
          params[:address]
        else
          nil
        end
      end
    end
  end
end
