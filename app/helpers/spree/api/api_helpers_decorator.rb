module Spree
  module Api
    module ApiHelpers

      mattr_reader :shop_attributes

      @@shop_attributes = [
          :id, :name, :address,
          :latitude, :longitude,
          :public_phone, :photo, :rating,
          :opened, :service_hours_weekdays, :service_hours_friday, :service_hours_saturday
      ]

      @@taxon_attributes = [
          :id, :name, :pretty_name, :permalink, :parent_id,
          :taxonomy_id
      ]

      @@product_attributes = [
          :id, :name, :description, :slug
      ]

      @@variant_attributes = [
          :id, :name, :sku, :price, :weight, :height, :width, :depth, :is_master,
          :cost_price, :slug, :description, :track_inventory
      ]

      @@order_attributes = [
          :id, :number, :shop_id, :item_total, :total, :ship_total, :state, :adjustment_total,
          :user_id, :created_at, :updated_at, :completed_at, :payment_total,
          :shipment_state, :payment_state, :email, :special_instructions, :channel,
          :included_tax_total, :additional_tax_total, :display_included_tax_total,
          :display_additional_tax_total, :tax_total, :currency
      ]

      @@user_attributes = [:id, :email, :spree_api_key]

      @@address_attributes = [
          :id, :firstname, :lastname, :full_name, :address1, :address2, :city,
          :zipcode, :phone, :country_name,
          :entrance, :apartment, :floor, :delivery_instructions
      ]

      @@country_attributes = [:id, :iso_name, :iso, :iso3, :name, :numcode]

      @@state_attributes = [:id, :name, :abbr, :country_id]

    end
  end
end