module Spree
  module BaseHelper
    def spree_current_shop
      if !@shop.nil?
         @shop
      elsif spree_current_user.respond_to?(:has_spree_role?) && spree_current_user.has_spree_role?('manager')
        unless spree_current_user.shop.nil?
          spree_current_user.shop
        end
      end
    end

    def display_shop_price(product)
      shop = @shop.nil? ? spree_current_shop : @shop
      product.display_shop_price(shop).to_html
    end

    def seo_url(taxon)
      return spree.nested_shop_taxons_path(@shop.id, taxon.permalink)
    end

  end
end