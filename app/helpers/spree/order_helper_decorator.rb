module Spree
  module Core
    module ControllerHelpers
      module Order

        def current_order(options = {})
          options[:create_order_if_necessary] ||= false
          options[:lock] ||= false

          return @current_order if @current_order

          # Find any incomplete orders for the guest_token
          @current_order = Spree::Order.includes(:adjustments).lock(options[:lock]).find_by(completed_at: nil, currency: current_currency, guest_token: cookies.signed[:guest_token])

          if options[:create_order_if_necessary] and (@current_order.nil? or @current_order.completed?)
            @current_order = Spree::Order.new(currency: current_currency, shop_id: params[:shop_id], guest_token: cookies.signed[:guest_token])
            @current_order.user ||= try_spree_current_user
            # See issue #3346 for reasons why this line is here
            @current_order.created_by ||= try_spree_current_user
            @current_order.save!
          end

          if @current_order
            @current_order.last_ip_address = ip_address
            return @current_order
          end
        end

      end
    end
  end
end
