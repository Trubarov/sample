object @shop

attributes *shop_attributes

child(@products => :products) do
  extends "spree/api/products/show"
end
