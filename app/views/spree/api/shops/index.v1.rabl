object false
child @shops => :shops do
  object @shop
  attributes *shop_attributes
  node(:distance_from_user) { |shop| shop.rounded_distance_from(@user_location) }
end
