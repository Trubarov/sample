object @product
cache [I18n.locale, current_currency, root_object]
attributes *product_attributes
node(:shop_price) { |p| p.display_shop_price(@shop).to_s }
child(:images => :images) { extends "spree/api/images/show" }
