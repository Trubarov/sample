# Configure Spree Preferences
#
# Note: Initializing preferences available within the Admin will overwrite any changes that were made through the user interface when you restart.
#       If you would like users to be able to update a setting with the Admin it should NOT be set here.
#
# In order to initialize a setting do:
# config.setting_name = 'new value'
Spree.config do |config|
  # Example:
  # Uncomment to stop tracking inventory levels in the application
  # config.track_inventory_levels = false

  config.admin_interface_logo = 'logo.png'
  config.logo = 'logo.png'

  config.default_country_id = 22    #Israel
  config.address_requires_state = false
end

Spree.user_class = "Spree::User"
