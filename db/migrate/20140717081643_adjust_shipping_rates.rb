class AdjustShippingRates < ActiveRecord::Migration
  def change
    eu_vat = Spree::Zone.find_by_name!("EU_VAT")

    # Add Israel to default tax zone
    eu_vat.zone_members.create!(zoneable: Spree::Country.find_by!(name: "Israel"))

    # Create tax category and rate
    tax_category = Spree::TaxCategory.create!(:name => "Default")
    tax_rate = Spree::TaxRate.create(
        :name => "Default",
        :zone => eu_vat,
        :amount => 0,
        :tax_category => tax_category)
    tax_rate.calculator = Spree::Calculator::DefaultTax.create!
    tax_rate.save!

    # Create shipping method
    shipping_category = Spree::ShippingCategory.find_or_create_by!(name: "Default")

    shipping_method = Spree::ShippingMethod.create!({
      :name => "Default",
      :zones => [eu_vat],
      :calculator => Spree::Calculator::Shipping::FlatRate.create!,
      :shipping_categories => [shipping_category]
    })

    shipping_method.calculator.preferences = {
        amount: 0,
        currency: "ILS"
    }
    shipping_method.calculator.save!
    shipping_method.save!


    # Adjust product tax category
    Spree::Product.all.each do |product|
      product.tax_category = tax_category
      product.save!
    end

  end
end
