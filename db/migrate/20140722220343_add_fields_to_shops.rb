class AddFieldsToShops < ActiveRecord::Migration
  def change
    add_column :shops, :active, :boolean, :default => true
    add_column :shops, :owner_name, :string
    add_column :shops, :email, :string
    add_column :shops, :public_phone, :string
    add_column :shops, :private_phone, :string
    add_column :shops, :rating, :decimal, :precision => 8, :scale => 2
    add_column :shops, :service_hours_weekdays, :string
    add_column :shops, :service_hours_friday, :string
    add_column :shops, :service_hours_saturday, :string
    add_column :shops, :payment_details, :string
    add_column :shops, :min_delivery_time, :integer
    add_column :shops, :serving_area_radius, :integer

    add_column :shops, :photo_file_name, :string
    add_column :shops, :photo_content_type, :string
    add_column :shops, :photo_file_size, :integer
    add_column :shops, :photo_updated_at, :datetime

  end
end
