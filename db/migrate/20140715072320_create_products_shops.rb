class CreateProductsShops < ActiveRecord::Migration
  def change
    create_table :products_shops do |t|
      t.references :product
      t.references :shop

      t.decimal :price, :precision => 8, :scale => 2
      t.boolean :available, :default => false

    end
  end
end
