class ChangeAssociationBetweenShopAndUser < ActiveRecord::Migration
  def change
    drop_table :shops_users
    add_column :shops, :user_id, :integer
  end
end
