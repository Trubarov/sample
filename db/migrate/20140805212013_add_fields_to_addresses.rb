class AddFieldsToAddresses < ActiveRecord::Migration
  def change
    add_column :spree_addresses, :entrance, :string, :default => ''
    add_column :spree_addresses, :apartment, :string, :default => ''
    add_column :spree_addresses, :floor, :string, :default => ''
    add_column :spree_addresses, :delivery_instructions, :string, :default => ''
  end
end
