class AddShopIdToOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :shop_id, :integer
  end
end
