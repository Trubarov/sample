class AddManagerRole < ActiveRecord::Migration
  def up
    Spree::Role.where(:name => 'manager').first_or_create
  end

  def down
    Spree::Role.where(:name => 'manager').delete
  end
end
