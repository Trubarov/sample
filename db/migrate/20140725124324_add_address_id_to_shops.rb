class AddAddressIdToShops < ActiveRecord::Migration
  def change
    add_column :shops, :address_id, :integer
    remove_column :shops, :address

    add_column :shops, :longitude, :decimal, :precision => 15, :scale => 10, :default => 0.0
    add_column :shops, :latitude, :decimal, :precision => 15, :scale => 10, :default => 0.0
  end
end
