class AddGuestUser < ActiveRecord::Migration
  def change
    token = SecureRandom.hex(24)
    guest = Spree::User.create({:email => 'guest@example.net', :password => token, :password_confirmation => token})
    guest.generate_spree_api_key!
  end
end
